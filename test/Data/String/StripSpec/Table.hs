{-# LANGUAGE OverloadedStrings #-}

-- | Translated from https://github.com/chalk/ansi-regex/blob/166a0d5eddedacf0db7ccd7ee137b862ab1dae70/fixtures/ansi-codes.js

module Data.String.StripSpec.Table
  ( ansiCodeTable
  , Entry (..)
  ) where

import           Data.Text (Text)

data Entry = Entry
  { entryCode    :: Text
  , entryComment :: String
  } deriving (Eq, Show)


(=:) :: a -> b -> (a, b)
(=:) = (,)

ansiCodeTable :: [(String, [Entry])]
ansiCodeTable =
  [ "vt52Codes" =: -- http://www.umich.edu/~archive/apple2/misc/programmers/vt100.codes.txt
    [ Entry "A" "Cursor up"
    , Entry "B" "Cursor down"
    , Entry "C" "Cursor right"
    , Entry "D" "Cursor left"
    , Entry "H" "Cursor to home"
    , Entry "I" "Reverse line feed"
    , Entry "J" "Erase to end of screen"
    , Entry "K" "Erase to end of line"
    , Entry "S" "Scroll up"
    , Entry "T" "Scroll down"
    , Entry "Z" "Identify"
    , Entry "=" "Enter alternate keypad mode"
    , Entry ">" "Exit alternate keypad mode"
    , Entry "1" "Graphics processor on"
    , Entry "2" "Graphics processor off"
    , Entry "<" "Enter ANSI mode"
    ]
  , "ansiCompatible" =: -- http://www.umich.edu/~archive/apple2/misc/programmers/vt100.codes.txt
    [ Entry "[176A" "Cursor up Pn lines"
    , Entry "[176B" "Cursor down Pn lines"
    , Entry "[176C" "Cursor forward Pn characters (right)"
    , Entry "[176D" "Cursor backward Pn characters (left)"
    , Entry "[176;176H" "Direct cursor addressing, where Pl is line#, Pc is column#"
    , Entry "[176;176f" "Direct cursor addressing, where Pl is line#, Pc is column#"

    , Entry "7" "Save cursor and attributes"
    , Entry "8" "Restore cursor and attributes"

    , Entry "#3" "Change this line to double-height top half"
    , Entry "#4" "Change this line to double-height bottom half"
    , Entry "#5" "Change this line to single-width single-height"
    , Entry "#6" "Change this line to double-width single-height"

    , Entry "[176;176;176;176;176;176;176m" "Text Styles"
    , Entry "[176;176;176;176;176;176;176q" "Programmable LEDs"

    , Entry "[K" "Erase from cursor to end of line"
    , Entry "[0K" "Same"
    , Entry "[1K" "Erase from beginning of line to cursor"
    , Entry "[2K" "Erase line containing cursor"
    , Entry "[J" "Erase from cursor to end of screen"
    , Entry "[0J" "Same"
    , Entry "[2J" "Erase entire screen"
    , Entry "[P" "Delete character"
    , Entry "[0P" "Delete character (0P)"
    , Entry "[2P" "Delete 2 characters"

    , Entry "(A" "United Kingdom (UK) (Character Set G0)"
    , Entry ")A" "United Kingdom (UK) (Character Set G1)"
    , Entry "(B" "United States (USASCII) (Character Set G0)"
    , Entry ")B" "United States (USASCII) (Character Set G1)"
    , Entry "(0" "Special graphics/line drawing set (Character Set G0)"
    , Entry ")0" "Special graphics/line drawing set (Character Set G1)"
    , Entry "(1" "Alternative character ROM (Character Set G0)"
    , Entry ")1" "Alternative character ROM (Character Set G1)"
    , Entry "(2" "Alternative graphic ROM (Character Set G0)"
    , Entry ")2" "Alternative graphic ROM (Character Set G1)"

    , Entry "H" "Set tab at current column"
    , Entry "[g" "Clear tab at current column"
    , Entry "[0g" "Same"
    , Entry "[3g" "Clear all tabs"

    , Entry "[6n" "Cursor position report"
    , Entry "[176;176R" "(response; Pl=line#; Pc=column#)"
    , Entry "[5n" "Status report"
    , Entry "[c" "(response; terminal Ok)"
    , Entry "[0c" "(response; teminal not Ok)"
    , Entry "[?1;176c" "response; where Ps is option present:"

    , Entry "c" "Causes power-up reset routine to be executed"
    , Entry "#8" "Fill screen with \"E\""
    , Entry "[2;176y" "Invoke Test(s), where Ps is a decimal computed by adding the numbers of the desired tests to be executed"
    ]

  , "commonCodes" =: -- http://ascii-table.com/ansi-escape-sequences-vt-100.php
    [ Entry "[176A" "Move cursor up n lines"
    , Entry "[176B" "Move cursor down n lines"
    , Entry "[176C" "Move cursor right n lines"
    , Entry "[176D" "Move cursor left n lines"
    , Entry "[176;176H" "Move cursor to screen location v,h"
    , Entry "[176;176f" "Move cursor to screen location v,h"
    , Entry "[176;176r" "Set top and bottom lines of a window"
    , Entry "[176;176R" "Response: cursor is at v,h"

    , Entry "[?1;1760c" "Response: terminal type code n"

    , Entry "[20h" "Set new line mode"
    , Entry "[?1h" "Set cursor key to application"
    , Entry "[?3h" "Set number of columns to 132"
    , Entry "[?4h" "Set smooth scrolling"
    , Entry "[?5h" "Set reverse video on screen"
    , Entry "[?6h" "Set origin to relative"
    , Entry "[?7h" "Set auto-wrap mode"
    , Entry "[?8h" "Set auto-repeat mode"
    , Entry "[?9h" "Set interlacing mode"
    , Entry "[20l" "Set line feed mode"
    , Entry "[?1l" "Set cursor key to cursor"
    , Entry "[?2l" "Set VT52 (versus ANSI)"
    , Entry "[?3l" "Set number of columns to 80"
    , Entry "[?4l" "Set jump scrolling"
    , Entry "[?5l" "Set normal video on screen"
    , Entry "[?6l" "Set origin to absolute"
    , Entry "[?7l" "Reset auto-wrap mode"
    , Entry "[?8l" "Reset auto-repeat mode"
    , Entry "[?9l" "Reset interlacing mode"

    , Entry "N" "Set single shift 2"
    , Entry "O" "Set single shift 3"

    , Entry "[m" "Turn off character attributes"
    , Entry "[0m" "Turn off character attributes"
    , Entry "[1m" "Turn bold mode on"
    , Entry "[2m" "Turn low intensity mode on"
    , Entry "[4m" "Turn underline mode on"
    , Entry "[5m" "Turn blinking mode on"
    , Entry "[7m" "Turn reverse video on"
    , Entry "[8m" "Turn invisible text mode on"

    , Entry "[9m" "strikethrough on"
    , Entry "[22m" "bold off (see below)"
    , Entry "[23m" "italics off"
    , Entry "[24m" "underline off"
    , Entry "[27m" "inverse off"
    , Entry "[29m" "strikethrough off"
    , Entry "[30m" "set foreground color to black"
    , Entry "[31m" "set foreground color to red"
    , Entry "[32m" "set foreground color to green"
    , Entry "[33m" "set foreground color to yellow"
    , Entry "[34m" "set foreground color to blue"
    , Entry "[35m" "set foreground color to magenta (purple)"
    , Entry "[36m" "set foreground color to cyan"
    , Entry "[37m" "set foreground color to white"
    , Entry "[39m" "set foreground color to default (white)"
    , Entry "[40m" "set background color to black"
    , Entry "[41m" "set background color to red"
    , Entry "[42m" "set background color to green"
    , Entry "[43m" "set background color to yellow"
    , Entry "[44m" "set background color to blue"
    , Entry "[45m" "set background color to magenta (purple)"
    , Entry "[46m" "set background color to cyan"
    , Entry "[47m" "set background color to white"
    , Entry "[49m" "set background color to default (black)"

    , Entry "[H" "Move cursor to upper left corner"
    , Entry "[;H" "Move cursor to upper left corner"
    , Entry "[f" "Move cursor to upper left corner"
    , Entry "[;f" "Move cursor to upper left corner"
    , Entry "M" "Move/scroll window down one line"
    , Entry "E" "Move to next line"

    , Entry "H" "Set a tab at the current column"
    , Entry "[g" "Clear a tab at the current column"
    , Entry "[0g" "Clear a tab at the current column"
    , Entry "[3g" "Clear all tabs"

    , Entry "[K" "Clear line from cursor right"
    , Entry "[0K" "Clear line from cursor right"
    , Entry "[1K" "Clear line from cursor left"
    , Entry "[2K" "Clear entire line"
    , Entry "[J" "Clear screen from cursor down"
    , Entry "[0J" "Clear screen from cursor down"
    , Entry "[1J" "Clear screen from cursor up"
    , Entry "[2J" "Clear entire screen"

    , Entry "[c" "Identify what terminal type"
    , Entry "[0c" "Identify what terminal type (another)"
    , Entry "c" "Reset terminal to initial state"
    , Entry "[2;1y" "Confidence power up test"
    , Entry "[2;2y" "Confidence loopback test"
    , Entry "[2;9y" "Repeat power up test"
    , Entry "[2;10y" "Repeat loopback test"
    , Entry "[0q" "Turn off all four leds"
    , Entry "[1q" "Turn on LED #1"
    , Entry "[2q" "Turn on LED #2"
    , Entry "[3q" "Turn on LED #3"
    , Entry "[4q" "Turn on LED #4"
    ]

  , "otherCodes" =: -- http://ascii-table.com/ansi-escape-sequences-vt-100.php
    [ Entry "7" "Save cursor position and attributes"
    , Entry "8" "Restore cursor position and attributes"

    , Entry "=" "Set alternate keypad mode"
    , Entry ">" "Set numeric keypad mode"

    , Entry "(A" "Set United Kingdom G0 character set"
    , Entry ")A" "Set United Kingdom G1 character set"
    , Entry "(B" "Set United States G0 character set"
    , Entry ")B" "Set United States G1 character set"
    , Entry "(0" "Set G0 special chars. & line set"
    , Entry ")0" "Set G1 special chars. & line set"
    , Entry "(1" "Set G0 alternate character ROM"
    , Entry ")1" "Set G1 alternate character ROM"
    , Entry "(2" "Set G0 alt char ROM and spec. graphics"
    , Entry ")2" "Set G1 alt char ROM and spec. graphics"

    , Entry "#3" "Double-height letters, top half"
    , Entry "#4" "Double-height letters, bottom half"
    , Entry "#5" "Single width, single height letters"
    , Entry "#6" "Double width, single height letters"
    , Entry "#8" "Screen alignment display"

    , Entry "5n" "Device status report"
    , Entry "0n" "Response: terminal is OK"
    , Entry "3n" "Response: terminal is not OK"
    , Entry "6n" "Get cursor position"
    ]

  , "urxvtCodes" =: -- https://linux.die.net/man/7/urxvt
    [ Entry "[5~" "URxvt.keysym.Prior"
    , Entry "[6~" "URxvt.keysym.Next"
    , Entry "[7~" "URxvt.keysym.Home"
    , Entry "[8~" "URxvt.keysym.End"
    , Entry "[A" "URxvt.keysym.Up"
    , Entry "[B" "URxvt.keysym.Down"
    , Entry "[C" "URxvt.keysym.Right"
    , Entry "[D" "URxvt.keysym.Left"
    , Entry "[3;5;5t" "URxvt.keysym.C-M-q"
    , Entry "[3;5;606t" "URxvt.keysym.C-M-y"
    , Entry "[3;1605;5t" "URxvt.keysym.C-M-e"
    , Entry "[3;1605;606t" "URxvt.keysym.C-M-c"
    , Entry "]710;9x15bold\x0007" "URxvt.keysym.font"
    ]
  ]
