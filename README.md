# strip-ansi-escape

Strip ANSI escape code from string. Haskell port of <https://github.com/chalk/strip-ansi>.
