module Data.String.AnsiEscapeCodes.Strip.Text
  ( stripAnsiEscapeCodes
  ) where

import qualified Data.Attoparsec.Text                       as AT
import qualified Data.Text                                  as T

import           Data.String.AnsiEscapeCodes.Strip.Internal

{-# INLINE stripAnsiEscapeCodes #-}
stripAnsiEscapeCodes :: T.Text -> T.Text
stripAnsiEscapeCodes str =
  either (const str) id $ AT.parseOnly (stripAnsiEscapeCodesP e) str
 where
  e = Env AT.skipWhile AT.takeWhile1
