module Data.String.AnsiEscapeCodes.Strip.Text.Lazy
  ( stripAnsiEscapeCodes
  ) where

import qualified Data.Attoparsec.Text.Lazy                  as AT
import qualified Data.Text.Lazy                             as T

import           Data.String.AnsiEscapeCodes.Strip.Internal

{-# INLINE stripAnsiEscapeCodes #-}
stripAnsiEscapeCodes :: T.Text -> T.Text
stripAnsiEscapeCodes str =
  case AT.parse (stripAnsiEscapeCodesP e) str of
      AT.Done _ r -> T.fromStrict r
      AT.Fail {}  -> str
 where
  e = Env AT.skipWhile AT.takeWhile1
